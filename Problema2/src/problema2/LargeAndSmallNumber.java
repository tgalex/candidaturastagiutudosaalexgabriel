package problema2;

public class LargeAndSmallNumber {
	public void LargestNumber(int lista[]){
		int largestnum=0;
		for(int i=0;i<lista.length;i++){
			if(lista[i]>largestnum){
				largestnum=lista[i];
			}
		}
		System.out.println("Largest Number is: "+largestnum); 
	}
	
	public void SmallestNumber(int lista[]){
		int smallestnum=lista[0];
		for(int i=1;i<lista.length;i++){
			if(lista[i]<smallestnum){
				smallestnum=lista[i];
			}
		}
		System.out.println("Smallest Number is: "+smallestnum); 
		
	}

}
