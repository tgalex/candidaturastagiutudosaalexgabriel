package problema2;

import java.util.ArrayList;

public class PrimeNum {
	public void ShowPrime(int lista[]){
		ArrayList<Integer> prime=new ArrayList<Integer>();
		for(int i=0;i<lista.length;i++){
			if(lista[i]%2!=0 && lista[i]!=0 && lista[i]!=1){
				System.out.print(lista[i]+",");
				prime.add(lista[i]);
			}
		}
		System.out.println("\nPrime numbers: "+prime);
	}

}
